CREATE DATABASE if not exists lager CHAR SET = utf8;
USE lager;

CREATE TABLE if not EXISTS tbl_artikel (
aID INTEGER NOT NULL AUTO_INCREMENT,
aName varchar(255) NOT NULL,
unique(aID, aName),
PRIMARY KEY(aID)
);

-- INSERT INTO tbl_artikel(aName) VALUES ('Produkt A');
-- INSERT INTO tbl_artikel(aName) VALUES ('Produkt B');
-- INSERT INTO tbl_artikel(aName) VALUES ('Produkt C');
-- INSERT INTO tbl_artikel(aName) VALUES ('Produkt D');

SELECT * FROM tbl_artikel;
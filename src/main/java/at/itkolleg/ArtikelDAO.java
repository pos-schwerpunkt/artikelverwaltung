package at.itkolleg;

import java.util.ArrayList;

public interface ArtikelDAO {
    public ArrayList<Artikel> getAll();
    public Artikel insert(Artikel a);
    public void update(int id, Artikel a);
    public void delete(int id);
    public Artikel getByID(int id);
}

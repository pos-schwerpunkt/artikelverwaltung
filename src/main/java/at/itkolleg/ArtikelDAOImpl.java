package at.itkolleg;

import java.util.ArrayList;

public class ArtikelDAOImpl extends SqlConnection implements ArtikelDAO {

    @Override
    public ArrayList<Artikel> getAll() {
        for (Artikel a : showArtikel()) {
            System.out.println(a);
        }
        return showArtikel();
    }

    @Override
    public Artikel insert(Artikel a) {
        return insertArtikel(a);
    }

    @Override
    public void update(int id, Artikel a) {
        updateArtikel(id, a);
    }

    @Override
    public void delete(int id) {
        deleteArtikel(id);
    }

    @Override
    public Artikel getByID(int id) {
        return getArtikelById(id);

    }
}

package at.itkolleg;

public class App {

    private static Artikel a1 = new Artikel("Produkt x");
    private static Artikel a2 = new Artikel("Produkt Y");
    private static Artikel a3 = new Artikel("Produkt Z");

    public static void main( String[] args ) {
        ArtikelDAO dao = new ArtikelDAOImpl();
        dao.insert(a1);
        dao.insert(a2);
        dao.getAll();
        System.out.println("updated artikel");
        dao.update(2, a3);
        dao.getAll();
        System.out.println("Get Artikel by id:");
        System.out.println(dao.getByID(2));
        System.out.println("deleted artikel");
        dao.delete(2);
        dao.getAll();
    }
}

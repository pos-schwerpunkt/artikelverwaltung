package at.itkolleg;

import java.sql.*;
import java.util.ArrayList;

public class SqlConnection {

    private final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private final String URL = "jdbc:mysql://127.0.0.1:3306/lager";
    private final String USER = "root";
    private final String PASS = "";
    private final String SELECT = "SELECT * FROM tbl_artikel";
    private final String SELECTBYID = "SELECT * FROM tbl_artikel WHERE aID=?";
    private final String INSERT = "INSERT INTO tbl_artikel (aName) VALUES (?)";
    private final String UPDATE = "UPDATE tbl_artikel SET aName=? WHERE aID=?";
    private final String DELETE = "DELETE FROM tbl_artikel WHERE aID =?";

    public Statement Statement() throws SQLException, ClassNotFoundException {
        Class.forName(DRIVER);
        Connection con = DriverManager.getConnection(URL, USER, PASS);
        Statement stmt = con.createStatement();
        return stmt;
    }

    public Connection Connection() throws SQLException {
        Connection con = DriverManager.getConnection(URL, USER, PASS);
        return con;
    }

    public ArrayList<Artikel> showArtikel() {
        ArrayList<Artikel> list = new ArrayList<>();
        Artikel a;
        try {
            ResultSet rs = Statement().executeQuery(SELECT);
            ResultSetMetaData meta = rs.getMetaData();
            while (rs.next()) {
                int a1 = rs.getInt("aID");
                String b = rs.getString("aName");
                a = new Artikel(a1, b);
                list.add(a);
            }

            Connection().close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public Artikel getArtikelById(int id) {
        Artikel a = null;
        try {
            PreparedStatement ps = Connection().prepareStatement(SELECTBYID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int a1 = rs.getInt("aID");
                String b = rs.getString("aName");
                a = new Artikel(a1, b);
            }
            Connection().close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return a;
    }

    public Artikel insertArtikel(Artikel a) {
        try {
            PreparedStatement ps = Connection().prepareStatement(INSERT);
            ps.setString(1, a.getName());
            ps.executeUpdate();
        } catch (SQLIntegrityConstraintViolationException e) {
            System.out.println("#############################");
            System.out.println("No duplicate entries allowed!");
            System.out.println("#############################");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }

    public void updateArtikel(int id, Artikel a) {
        try {
            PreparedStatement ps = Connection().prepareStatement(UPDATE);
            ps.setString(1, a.getName());
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteArtikel(int id) {
        try {
            PreparedStatement ps = Connection().prepareStatement(DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
